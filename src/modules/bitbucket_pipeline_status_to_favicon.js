(function() {

  const SETTINGS_KEY = 'bitbucket_pipeline_status_to_favicon';

  chrome.storage.sync.get(SETTINGS_KEY, (data) => {
    if(data[SETTINGS_KEY]) {
      const ICON_SELECTOR = '.build-status-icon';
      const VETO_SELECTOR = '.veto-count';
      const LINK_ELEMENT_ID = '[rel="shortcut icon"]';
      const SUMMARY_PANEL = 'summary-panel';
      const MERGE_BUTTON = 'merge-button-container';

      function pipelineStatusToFavicon() {

          const faviconSvg = document.createElement('svg');
          faviconSvg.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

          // TODO[perf]: this should not target whole document at this point
          // TODO: the .item(1) might be a bit brittle
          const statusTitle = document.querySelector(ICON_SELECTOR)?.classList?.item(1); 
          const faviconTextEl = document.createElement('text');
          faviconTextEl.setAttribute('font-size', '32');
          faviconTextEl.setAttribute('y', '28');
          const icons = {
            'build-successful-icon': '🟩',
            'build-failed-icon': '🟥',
            'build-in-progress-icon': '🟦',
          };
          faviconTextEl.textContent = icons[statusTitle] || '❓';
          faviconSvg.appendChild(faviconTextEl);

          // TODO[perf]: this should not target whole document at this point
          const vetoCount = Number.parseInt(document.querySelector(VETO_SELECTOR)?.textContent) || 0;
          if(vetoCount > 0) {
            const vetoCountTextEl = document.createElement('text');
            vetoCountTextEl.setAttribute('font-size', '24');
            vetoCountTextEl.setAttribute('font-family', 'monospace');
            vetoCountTextEl.setAttribute('fill', 'white');
            vetoCountTextEl.setAttribute('x', '8');
            vetoCountTextEl.setAttribute('y', '24');
            vetoCountTextEl.textContent = vetoCount;
            faviconSvg.appendChild(vetoCountTextEl);
          }

          let linkEl = document.head.querySelector(LINK_ELEMENT_ID);
          linkEl.href = `data:image/svg+xml,${faviconSvg.outerHTML}`;
      }

      const observer = new MutationObserver((mutations) => {
        mutations.forEach((mutation) => {
          // TODO: looking for two things in one is not ideal
          // but prevents exploring mutations that are not "ours"
          // maybe there is a better way?
          if(
              mutation.target.className === SUMMARY_PANEL || 
              mutation.target.className === MERGE_BUTTON
          ) {
            if (
              mutation.target.nodeType === Node.ELEMENT_NODE && 
              (
                mutation.target.querySelector(ICON_SELECTOR) || 
                mutation.target.querySelector(VETO_SELECTOR)
              )
            ) {
              pipelineStatusToFavicon();
            }
          }
        });
      });

      const config = {
        subtree: true,
        attributes: true,
        childList: true,
        characterData: true,
      };

      observer.observe(document.body, config);
    }
  });

}());