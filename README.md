## Bitbucket AliceBlue

Chrome extension to improve Bitbucket experience.

### Screenshots

Showcase of current features going from the left:

- CI pipeline is running, there are 6 vetoes on the PR.
- CI pipeline run has failed, there are 6 vetoes on the PR.
- CI pipeline run was successful, but there are still 2 vetoes on the PR.
- CI pipeline run was successful and there are no other vetoes on the PR.

![Bitbucket AliceBlue showcase](./docs/showcase.png)

Options page:

![Options page](./docs/options.png)

### Modules

#### Bitbucket pipeline status to favicon

Changes favicon of currently open browser tab to match pipeline status.
According to following key:

```
successful: '🟩',
failed: '🟥',
in-progress: '🟦',
```

Additionally, if present, it shows the number `N` of the `There are N issues preventing your from merging this pull request in said favicon.

### Installation

- Chrome Web Store publication is pending ⏳

#### Manual installation

1. Install [volta](https://volta.sh/)
2. Build extension:
```sh
yarn install
yarn build
```
3. [Load unpacked](https://developer.chrome.com/docs/extensions/mv3/getstarted/#unpacked) extension to Chrome